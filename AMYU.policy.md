### Quyền truy cập và thu thập dữ liệu
Ứng dụng yêu cầu quyền truy cập vào thư viện ảnh và bộ nhớ thiết bị của bạn để cho phép chỉnh sửa và lưu trữ ảnh cũng như sản phẩm photobook. Chúng tôi cũng có thể thu thập thông tin cá nhân cơ bản như tên, email và địa chỉ giao hàng nếu bạn sử dụng tính năng đặt hàng photobook. Dữ liệu này chỉ được sử dụng để cung cấp dịch vụ tạo và giao hàng photobook.

### Lưu trữ và bảo mật dữ liệu
Thông tin cá nhân của bạn được lưu trữ trên máy chủ an toàn có áp dụng biện pháp mã hóa và các biện pháp bảo mật khác để bảo vệ dữ liệu khỏi truy cập trái phép. Chúng tôi cam kết tuân thủ các tiêu chuẩn bảo mật nghiêm ngặt để đảm bảo dữ liệu của bạn được xử lý và lưu trữ an toàn.

### Quyền của người dùng
Bạn có quyền truy cập, sửa đổi và xóa dữ liệu cá nhân của mình trong ứng dụng. Nếu bạn có bất kỳ yêu cầu nào liên quan đến dữ liệu cá nhân, vui lòng liên hệ với chúng tôi thông qua email amyuphoto@gmail.com.

### Cập nhật chính sách
Chúng tôi có thể cập nhật chính sách bảo mật này vào bất kỳ thời điểm nào để phản ánh các thay đổi trong cách chúng tôi xử lý dữ liệu hoặc các yêu cầu pháp lý mới. Nếu có bất kỳ thay đổi quan trọng nào, chúng tôi sẽ thông báo cho bạn thông qua ứng dụng hoặc email.

### Thông tin liên hệ
Nếu bạn có bất kỳ câu hỏi hoặc lo ngại nào về chính sách bảo mật này hoặc cách chúng tôi xử lý dữ liệu cá nhân, vui lòng liên hệ với chúng tôi qua email amyuphoto@gmail.com.

Bằng cách sử dụng ứng dụng, bạn đồng ý với chính sách bảo mật này. Chúng tôi tôn trọng quyền riêng tư của bạn và cam kết bảo vệ dữ liệu cá nhân của bạn một cách an toàn.